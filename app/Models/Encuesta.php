<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Encuesta extends Model
{
    protected $table        = 'encuesta';
    protected $primaryKey   = 'id_encuesta';
    protected $fillable     = array('id_persona','id_servicio','fecha_nacimiento','contacto_estrecho_covid','sintomas_covid',
    'transporte','pase_movilidad','protocolo_covid','codigo');

    public function encuestaSend(){

        $sql='SELECT COUNT(*) as cantidad ,DATE_FORMAT(created_at, "%m %d %Y") as fecha1,DATE_FORMAT(created_at, "%M %d %Y") as fecha  FROM encuesta n
        WHERE DATE_FORMAT(n.CREATED_AT, "%Y-%m-%d") BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
        GROUP BY DATE_FORMAT(created_at, "%m %d %Y"),DATE_FORMAT(created_at, "%M %d %Y")';
        $result=DB::select($sql);
        
        return $result;

    }

    public function encuestaSendDay(){

        $sql="SELECT COUNT(*) as total FROM encuesta e
        WHERE DATE_FORMAT(e.CREATED_AT, '%Y-%m-%d')=CURDATE()";
        $result=DB::select($sql);

        return $result;

    }

    public function encuestaDiariaConPase(){
        
        $sql="SELECT COUNT(*) as total  FROM encuesta e
        WHERE DATE_FORMAT(e.CREATED_AT, '%Y-%m-%d')=CURDATE() 
        AND e.pase_movilidad=1";

        $result=DB::select($sql);

        return $result;

    }

    public function encuestaDiariaSinnPase(){
        
        $sql="SELECT COUNT(*) as total  FROM encuesta e
        WHERE DATE_FORMAT(e.CREATED_AT, '%Y-%m-%d')=CURDATE() 
        AND e.pase_movilidad=0";

        $result=DB::select($sql);

        return $result;

    }

    public function getEncuestaRut($rut){

        $sql="SELECT * FROM encuesta e INNER JOIN 
        persona p ON e.id_persona= p.id_persona 
        INNER JOIN servicio se ON e.id_servicio=se.id_servicio
        where p.rut='$rut'
        ORDER BY e.created_at,se.id_servicio,p.id_persona ";

        $result=DB::select($sql);

        return $result;


    }
    public function getInscritos($fecha,$servicio){

        $sql=" SELECT COUNT(*) as total FROM encuesta WHERE 
        CAST(fecha_ingreso AS DATE)='$fecha'
        AND id_servicio=$servicio";
        $result=DB::select($sql);

        return $result;


    }
   

    public function getEncuestaDate($desde,$hasta){

        $sql="SELECT * FROM encuesta e 
            INNER JOIN persona p ON e.id_persona=p.id_persona 
            INNER JOIN servicio se ON e.id_servicio=se.id_servicio WHERE
            CAST(fecha_ingreso AS DATE) BETWEEN CAST('$desde' AS DATE) 
            AND CAST('$hasta' AS DATE)
            ORDER BY e.created_at,se.id_servicio,p.id_persona ";

       

            $result=DB::select($sql);

            return $result;


    }
    
    public function validarPersonaInscripcion($fecha,$servicio,$idPersona){
            $sql="SELECT * FROM encuesta e WHERE
            e.id_persona='$idPersona'
            AND e.id_servicio=$servicio
            AND CAST(fecha_ingreso AS DATE)='$fecha'";
            $result=DB::select($sql);

             return $result;
     
            
        
    }

    public function list_incripcion($desde,$hasta,$rut){

        $sql="SELECT * FROM encuesta e 
            INNER JOIN persona p ON e.id_persona=p.id_persona 
            INNER JOIN servicio se ON e.id_servicio=se.id_servicio WHERE
            CAST(fecha_ingreso AS DATE) BETWEEN CAST('$desde' AS DATE) 
            AND CAST('$hasta' AS DATE)
            AND P.rut='$rut'
            ORDER BY e.created_at,se.id_servicio,p.id_persona ";

                 

            $result=DB::select($sql);

            return $result;


    }

    
}
