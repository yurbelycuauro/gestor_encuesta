<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table        = 'persona';
    protected $primaryKey   = 'id_persona';
    protected $fillable     = array('rut','nombre','direccion','fecha_nacimiento','id_persona');
}
