<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->percel_url = \config('app.PERCEL_URL');
        $this->percel_api_key = \config('app.PERCEL_API_KEY');
      
       
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
       
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            $userId = Auth::id();
            $currentuser = User::find($userId);
            $userId = Auth::id();
            $email = Auth::user()->email;
          
            Session::put('api_token', $request->session()->regenerate());
            $user=array('nombre'=>$currentuser->name,
                'apellido'=>"",'id_usurio'=> $currentuser->id,
                'token'=> $request->session()->regenerate());
                
                Session::put('nombre', $currentuser->name);
                Session::put('apellido', "");
                Session::put('id_usuario', $currentuser->id);
                Session::put('usurio', $user);
                Session::save();

            $code="200";

            $session=array('data'=>session()->all(),'code'=>$code);
            
            return $session;
        }
 
        return back()->withErrors([
            'mensaje' => 'The provided credentials do not match our records.',
        ]);
    }
    public function valid(Request $request)
    {
       //Auth::guard($guard)->check();
       $response = Http::withHeaders([
            'Authorization' => 'Bearer ' .  $this->percel_api_key
        ])->post($this->percel_url.'/api/user/login', [
            'user_name' => $request->all()['email'],
            'password' => $request->all()['password'],
        ]);

      
        $respuesta =json_decode($response->body());
        switch ($respuesta->code) {
            case 201:
            
                // OK
                Session::put('api_token', $respuesta->api_token);
                $user=array('nombre'=>$respuesta->first_name,
                'apellido'=>$respuesta->last_name,'id_usurio'=> $currentuser->id,
                'token'=>$respuesta->api_token);
                
                Session::put('nombre', $respuesta->first_name);
                Session::put('apellido', $respuesta->last_name);
                Session::put('id_usuario', $respuesta->user_id);
                Session::put('usurio', $user);
                Session::save();
               // $this->getMenu();

                $code="201";

                $session=array('data'=>session()->all(),'code'=>$code);
                
                return $session;
                break;
            case 303:
                // First login
                $usuario = $request->all()['email'];
                $contrasena = $request->all()['password'];
                $status = "303";
                return  compact('usuario', 'contrasena','status');
                break;
            case 401:
                // Bad credentials
                return  json_encode(['mensaje' => $respuesta->message]);
                return view('index')->with('mensaje', $respuesta->message);
                return redirect()->back()->with('mensaje', $respuesta->message);
                break;
            case 422:
                // Blocked user
                return  json_encode(['mensaje' => $respuesta->message]);
                return redirect()->back()->with('mensaje', $respuesta->message);
                break;
            default:
            
                // Unknow response
                break;
        }
       // return json_decode($response->body());
    }

    public function getMenu()
    {
        $percel_url = \config('app.PERCEL_URL');
        $percel_api_key = \config('app.PERCEL_API_KEY');
        try{
            $cliente = new Client(['base_uri' => $percel_url]);
            $peticion = $cliente->request(
                'POST',
                '/api/user/menu',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' .  $percel_api_key
                    ],
                    'form_params'  =>  [
                        'api_token' => Session::get('api_token')
                    ]
                ]);
            

            $respuesta = json_decode($peticion->getBody()->getContents());

           
            Session::put('api_token', $respuesta->api_token);
            Session::put('api_menu', $respuesta->menu);
            /*$hs = new Hs;
            $menu_html = $hs->menu($respuesta->menu);
            Session::put('menu', $menu_html);*/
           
            //

            //
        } catch (Exception $e) {
            \Log::debug($e);
            $response = json_decode($e->getResponse()->getBody());

            if($request->ajax() || $request->wantsJson()) {
                return response()->view('errors.422');
            } else {
                abort($e->getResponse()->getStatusCode(), $response->message);
            }
        }
    }
}
