<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use GuzzleHttp\Client;
use Exception;
use Session;


class ValidaCredencialesController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function validar(Request $request)
    {
        
        //Session::flush();
        try {

            // ******************************************************************************************************************
            $percel_url = \config('app.PERCEL_URL');
            $percel_api_key = \config('app.PERCEL_API_KEY');
            $cliente = new Client(['base_uri' => $percel_url]);
           
           
            
            $peticion = $cliente->request(
                    'POST', '/api/user/login', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $percel_api_key
                ],
                'form_params' => [
                    'user_name' => $request->all()['user_name'], 'password' => $request->all()['password']
                ]
            ]);
            $respuesta = json_decode($peticion->getBody()->getContents());

            
           
            
            switch ($respuesta->code) {
                case 201:
                
                    // OK
                    Session::put('api_token', $respuesta->api_token);
                    
                    Session::put('nombre', $respuesta->first_name);
                    Session::put('apellido', $respuesta->last_name);
                    Session::put('id_usuario', $respuesta->user_id);
                    Session::save();
                    $this->getMenu();
                   
                    $code=$respuesta->code;

                    $session=array('data'=>session()->all(),'code'=>$code);

                    return $session;

                    
                    break;
                case 303:
                    // First login
                    $usuario = $request->user_name;
                    $contrasena = $request->password;
                    $status = "303";
                    return  compact('usuario', 'contrasena','status');
                    break;
                case 401:
                    // Bad credentials
                    return  json_encode(['mensaje' => $respuesta->message]);
                    return view('index')->with('mensaje', $respuesta->message);
                    return redirect()->back()->with('mensaje', $respuesta->message);
                    break;
                case 422:
                    // Blocked user
                    return  json_encode(['mensaje' => $respuesta->message]);
                    return redirect()->back()->with('mensaje', $respuesta->message);
                    break;
                default:
                
                    // Unknow response
                    break;
            }
        } catch (Exception $e) {
            //\Log::debug($e);
           
            return redirect()->back()->with('flash_success', 'Problemas de acceso, contacte al administrador!');
            
        }
    }

    public function cambiarCredenciales(Request $request)
    {
        if ($request->all()['new_password'] === $request->all()['new_password_confirm']) {
            /*$percel_url = env('PERCEL_URL', '');
            $percel_api_key = env('PERCEL_API_KEY', '');*/

            $percel_url = \config('app.PERCEL_URL');
            $percel_api_key = \config('app.PERCEL_API_KEY');

            //$config = ParametroGeneral::first();
            $cliente = new Client(['base_uri' => $percel_url]);
            $peticion = $cliente->request(
                    'POST', '/api/user/newpass', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $percel_api_key
                ],
                'form_params' => [
                    'user_name' => $request->all()['user_name'],
                    'old_password' => $request->all()['old_password'],
                    'new_password' => $request->all()['new_password']
                ]
            ]);

            $respuesta = json_decode($peticion->getBody()->getContents());
            
            //print_r($respuesta);
            switch ($respuesta->code) {
                case 201:
                    // OK
                    Session::put('api_token', $respuesta->api_token);
                    Session::put('nombre', $respuesta->first_name);
                    Session::put('apellido', $respuesta->last_name);
                    Session::save();
                    

                    $this->getMenu();

                    $code=$respuesta->code;

                    $session=array('data'=>session()->all(),'code'=>$code);

                    return $session;
                    break;

                case 422:
                    // New password doesn't meet policy
                    $usuario = $request->user_name;
                    $contrasena = $request->old_password;
                    $mensaje = $respuesta->message;
                    $code =$respuesta->code;
                    
                    return  compact('usuario', 'contrasena', 'mensaje','code');
                    break;
                default:
                    // Unknow response
                    break;
            }
        } else {
            $mensaje ="verifique las credenciales";
            $code ="500";
            return compact('mensaje', 'code');
        }
    }

    public function forgot_password()
    {
        //return view('layouts/forgot_password');
    }

    public function recuperarCredenciales(Request $request)
    {
        $percel_url = \config('app.PERCEL_URL');
        $percel_api_key = \config('app.PERCEL_API_KEY');
        
        //$config = ParametroGeneral::first();
        $cliente = new Client(['base_uri' => $percel_url]);
        $peticion = $cliente->request(
                'POST', '/api/user/unlock', [
            'headers' => [
                'Authorization' => 'Bearer ' . $percel_api_key
            ],
            'form_params' => [
                'user_name' => $request->all()['user_name'],
            ]
        ]);

        $respuesta = json_decode($peticion->getBody()->getContents());
        

        $message = $respuesta->message;
       
        return  json_encode(['mensaje' => $message]);
        //return view('layouts/forgot_password', compact('tipo', 'message'));
    }

    public function getMenu()
    {
        $percel_url = \config('app.PERCEL_URL');
        $percel_api_key = \config('app.PERCEL_API_KEY');
        try{
            $cliente = new Client(['base_uri' => $percel_url]);
            $peticion = $cliente->request(
                'POST',
                '/api/user/menu',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' .  $percel_api_key
                    ],
                    'form_params'  =>  [
                        'api_token' => Session::get('api_token')
                    ]
                ]);
            

            $respuesta = json_decode($peticion->getBody()->getContents());

           
            Session::put('api_token', $respuesta->api_token);
            Session::put('api_menu', $respuesta->menu);
            /*$hs = new Hs;
            $menu_html = $hs->menu($respuesta->menu);
            Session::put('menu', $menu_html);*/
           
            //

            //
        } catch (Exception $e) {
            \Log::debug($e);
            $response = json_decode($e->getResponse()->getBody());

            if($request->ajax() || $request->wantsJson()) {
                return response()->view('errors.422');
            } else {
                abort($e->getResponse()->getStatusCode(), $response->message);
            }
        }
    }

}
