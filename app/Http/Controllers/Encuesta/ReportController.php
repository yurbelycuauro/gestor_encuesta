<?php

namespace App\Http\Controllers\Encuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Encuesta;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends Controller{

    public function __construct() {
        $this->meta = array('code'=>env('COD_META_REQUESTOK'),'message'=>env('MSG_META_REQUESTOK'));
    }

    public function dowload($tipo,$data)
    {
       
        $encuesta= new Encuesta;
               
        switch ($tipo) {
            case 'rut':
                $rut=$data;
                $campana= new Encuesta();
                $result=$campana->getEncuestaRut($rut);
                break;
            default:
                $data=json_decode($data);
                $desde=substr($data->desde, 0, 10);
                $hasta=substr($data->hasta, 0, 10);
                $result=$encuesta->getEncuestaDate($desde,$hasta);

              
        }
       
        
        $meta = $this->meta;
        return $this->generaExcel($result);
        
       
    }

    public function generaExcel($data)
    {
        $spreadsheet = new Spreadsheet();
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow()+1;
        $spreadsheet->getActiveSheet()->fromArray(['Fecha Ingreso','servicio','RUT','Nombre','Pase Movilidad','Contacto estrecho','Sintomas covid','Protocolo Covid'], null, 'A1' );
        foreach($data as $r){
            
            $fechaIngreso             = isset($r->fecha_ingreso) ? trim($r->fecha_ingreso) : '';
            $servicio                 = isset($r->descripicon) ? trim($r->descripicon) : ''; 
            $rut                      = isset($r->rut) ? trim($r->rut) : '';
            $nombre                   = isset($r->nombre) ? trim($r->nombre) : '';
            $pase_movilidad           = ($r->pase_movilidad)=='1' ? 'SI' : 'NO';
            $contacto_estrecho_covid  = ($r->contacto_estrecho_covid)=='1' ? 'SI' : 'NO';
            $sintomas_covid           = ($r->sintomas_covid)=='1' ? 'SI' : 'NO';
            $protocolo_covid          = ($r->protocolo_covid)=='1' ? 'SI' : 'NO';
           
            
            
            $array=array(
                $fechaIngreso,
                $servicio ,
                $rut ,
                $nombre  ,
                $pase_movilidad ,
                $contacto_estrecho_covid ,
                $sintomas_covid ,
                $protocolo_covid 
               
                 
            );
            
            $spreadsheet->getActiveSheet()->fromArray($array, null, 'A' . $highestRow++,true);
        }
        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '#000000'),
                'size'  => 9,
                'name'  => 'Verdana'
            ));
            $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
            
        ;
        

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        
        ob_end_clean (); // this 
        ob_start ();

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("php://output");
    }
}