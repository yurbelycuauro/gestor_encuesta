<?php

namespace App\Http\Controllers\Encuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Encuesta;
use App\Models\Servicio;

class EncuestaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }
    public function getEncuestaReportRut(Request $request){
        try {
            $rut=$request->all()[0];
            $campana= new Encuesta();
            $getEncuesta=$campana->getEncuestaRut($rut);
        return $getEncuesta;
            
        } catch (Exception $ex) {

        }
        
    }
    public function Encuesta(Request $request)
    {
        $data=$request->all();
        $rut=$data['rut'];
        $dataPersona=$this->validPersona($rut);
        if(count($dataPersona)<=0){
            $id=$this->savePersona($data);

        }else{
            $id=$dataPersona[0]['id_persona'];
        }
        $servicio=$data['pickedReunion'];
        $fecha=date('Y-m-d',strtotime($data['date']));
        $inscribir =$this->validarPersonaInscripcion($fecha,$servicio,$id);
        if($inscribir){
            $encuesta = new Encuesta();
            $encuesta->id_persona=$id;
            $encuesta->id_servicio=$data['pickedReunion'];
            $encuesta->fecha_ingreso=date('y-m-d h:i:s',strtotime($data['date']));
            $encuesta->contacto_estrecho_covid=$data['pickedContacto'];
            $encuesta->sintomas_covid=$data['pickedSintomas'];
            $encuesta->transporte=$data['pickedTransporte'];
            $encuesta->pase_movilidad=$data['pickedPase'];
            $encuesta->protocolo_covid=$data['pickedPCovid'];
            $encuesta->codigo=$data['code'];
            $encuesta->save();
            return json_encode(['data' => 'true']); 
        }else{
            return json_encode(['data' => 'false']); 
        }
       

    }

    public function validPersona($rut){

        $persona = Persona::where('rut', $rut)->get();
        return $persona;

    }

    public function savePersona($data){

        $persona = new Persona();
        $persona->nombre = $data['nombre'];
        $persona->rut = $data['rut'];
        $persona->save();
        return $persona->id_persona;

    }

    public function getEncuestaSend(Request $request){
        $encuesta= new Encuesta();
        $result=$encuesta->encuestaSend();
        $y=[];
        $x=[];
        foreach($result as $k=>$v){
            $y[]=$v->fecha;
            $x[]=$v->cantidad;
        }
        $data=array('eje_y'=>$y,'eje_x'=>$x);
        return $data;

    }

    public function getMonitorEncuesta(Request $request){
        
       
        $campana= new Encuesta();
        $encuestaSendDay=$campana->encuestaSendDay();
        $encuestaDiariaConPase=$campana->encuestaDiariaConPase();
        $encuestaDiariaSinnPase=$campana->encuestaDiariaSinnPase();
       
        $data= array('encuestaSendDay' => $encuestaSendDay,'encuestaDiariaConPase'=>$encuestaDiariaConPase,'encuestaDiariaSinnPase'=>$encuestaDiariaSinnPase );

        return $data;

    }

    public function validarInscripcion(Request $request){
        $data=$request->all();
        $fecha=date('Y-m-d',strtotime($data['date']));
        $servicio=$data['servicio'];
        $campana= new Encuesta();
        $inscritos=$campana->getInscritos($fecha,$servicio);

        $servicio = Servicio::find($servicio);
        
        $total =$inscritos[0]->total;
        $capacidad = $servicio->capacidad;
        if($total<$capacidad){
           return json_encode(['data' => 1]);
        }else{
            return json_encode(['data' => 2]);
        }
        
    }

    public function validarPersonaInscripcion($fecha,$servicio,$idPersona){
        $inscritos = new Encuesta();
        $inscrito=$inscritos->validarPersonaInscripcion($fecha,$servicio,$idPersona);
        if(count($inscrito)>0){
            return false;
        }else{
            return true;
        }
        return $inscrito;

    }

    public function getListEncuestaDate(Request $request){
        $encuesta= new Encuesta;
        $desde = $request->all()['desde'];
        $hasta = $request->all()['hasta'];
        $result=$encuesta->getEncuestaDate($desde,$hasta);
       
        return json_encode( $result  );;

    }

    public function list_incripcion(Request $request){
        $encuesta= new Encuesta;
        $desde = $request->all()['desde'];
        $hasta = $request->all()['hasta'];
        $rut = $request->all()['rut'];
        $result=$encuesta->list_incripcion($desde,$hasta,$rut);
       
        return json_encode( $result  );;

    }

    public function dowload(){
        
    }

    public function deleteInscripcion(Request $request){
        $id=$request->all()['id_encuesta'];
        $encuesta = Encuesta::find($id);
 
        $encuesta->delete();

        return true;;
    }

    

    
}
