import Http from "axios";

class Api {
    async get(url, params = {}) {
        const options = {
            params
        };

        const response = await Http.get(url, options);

        return response;
    }

    /**
     *
     * @param {String} url
     * @param {*} data
     * @param {*} header
     */
    async post(url, data, header = {}) {
        let token = document.head.querySelector('meta[name="csrf-token"]');
        if (token) {
             axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        } else {
            console.error('Hubo un problema cargando la Token CSRF.');
        }

        const options = {
            header
        };
        const response = await window.axios.post(url, data, options);
        return response;
    }
}

export default new Api();
