import Vue from "vue";

import VueRouter from "vue-router";
Vue.use(VueRouter);
import{InscripcionComponent,DesuscribirComponent} from '../components/index';
import{Login,ReportChart,ReportEncuesta} from '../modules';
import MainApp from "../MainApp";

import Store from "./../stores";
const routes = [
    { 
        path: "/",
        component:InscripcionComponent
    },
    { 
      path: "/gestor",
      name:"encuesta",
      component: Login 
    },
    { 
      path: "/cancelar",
      name:"desuscribir",
      component: DesuscribirComponent 
    },
    { 
        path: "/dashboard", 
        component: MainApp ,
        meta: {
          requiresAuth: true
        },
        redirect: "/dashboard",
        children:[
          {
            path: "/dashboard",
            name: "Chart_Api",
            component: ReportChart,
            meta: { requiresAuth: true },
          },
          {
            path: "/dashboard/reports",
            name: "reports_campana",
            component: ReportEncuesta,
            meta: { requiresAuth: true },
          },
        ]
    }
]

const router = new VueRouter({
    routes // short for `routes: routes`
  })

  router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = Store.getters["global/currentUser"];
    const isLoggedIn = Store.getters["global/isLoggedIn"];
    console.log("logginn",isLoggedIn);

    if (isLoggedIn) {
       /* axios.defaults.headers.common["Authorization"] = `Bearer ${
            currentUser.token
        }`;*/
    }

    if (requiresAuth && !isLoggedIn ) {
        next("/");
        console.log("logginnNOO");
    } else  {
        console.log("logginnSIIII");
        next();
    } 
    
});



export default router;