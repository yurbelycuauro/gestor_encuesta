import Api from "../libs/Api";
import { route } from "../helpers";

/**
 * Class ReportsService
 */
class ReportsService {

    
    /**
     *
     * @param string id
     */
    async getEncuestaReportRut(id) {
        const response = await Api.get("getEncuestaReportRut",id);

        return response.data;
    }

    /**
     *
     * @param string date
     *
     */
    async list_notificaciones_date(date) {
        const response = await Api.get("getListEncuestaDate",date);

        return response.data;
    }
    
    /**
     *
     * @param string date
     *
     */
    async list_incripcion(date) {
        const response = await Api.get("list_incripcion",date);

        return response.data;
    }

    
    
    
   
}

export default new ReportsService();
