import Api from "../libs/Api";
import { route } from "../helpers";

class EncuestaService{
     /**
     * Call the rules register api.
     * @param {Object} inputData
     */
    async save_encuesta(inputData) {
        const response = await Api.post("saveEncuesta",inputData);

        return response.data;
    }

    async validarInscripcion(inputData) {
        const response = await Api.post("validarInscripcion",inputData);

        return response.data;
    }
    async deleteInscripcion(inputData) {
        const response = await Api.post("deleteInscripcion",inputData);

        return response.data;
    }
    
}
export default new EncuestaService();