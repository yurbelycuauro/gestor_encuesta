import Api from "../libs/Api";
import { route } from "../helpers";

/**
 * Class ChartService
 */
class ChartService {
    /**
     *
     * @param string d
     */
    async chartEncuesta(d) {
        const response = await Api.get("chartEncuesta");

        return response.data;
    }
    /**
     *
     * @param string d
     */
    async reportEncuesta(d) {
        const response = await Api.get("reportEncuesta");

        return response.data;
    }


    
}

export default new ChartService();
