@extends('layouts.app')

@section('content')
    <main id="app" class=" h-100">
        <router-view class="container-fluid"></router-view>
    </main>
@endsection