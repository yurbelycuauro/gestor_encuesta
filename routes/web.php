<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::post('/validarCredendiales','UserController@authenticate' );
Route::post('/forgotCredendiales','ValidaCredencialesController@recuperarCredenciales' );
Route::post('/changeCredendiales','ValidaCredencialesController@cambiarCredenciales' );
Route::post('/saveEncuesta','Encuesta\EncuestaController@Encuesta' );
Route::post('/validarInscripcion','Encuesta\EncuestaController@validarInscripcion' );

/**********************Reportes************************ */

Route::get('/chartEncuesta','Encuesta\EncuestaController@getEncuestaSend' );
Route::get('/reportEncuesta','Encuesta\EncuestaController@getMonitorEncuesta' );
Route::get('/getEncuestaReportRut','Encuesta\EncuestaController@getEncuestaReportRut' );
Route::get('/getListEncuestaDate','Encuesta\EncuestaController@getListEncuestaDate' );
Route::get('/dowload/{tipo}/{data}','Encuesta\ReportController@dowload' );
Route::get('/list_incripcion','Encuesta\EncuestaController@list_incripcion' );

Route::post('/deleteInscripcion','Encuesta\EncuestaController@deleteInscripcion' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
